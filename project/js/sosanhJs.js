/**
 * Created by iL22 on 12/2/2018.
 */
var compare = [];
$(document).ready(function () {
    function Products(anh, nhanhieu, xuatsu, chungloai, congsuat, baohanh, gia) {
        this.Anh = anh;
        this.NhanHieu = nhanhieu;
        this.XuatSu = xuatsu;
        this.ChungLoai = chungloai;
        this.CongSuat = congsuat;
        this.BaoHanh = baohanh;
        this.Gia = gia;
    }

    var sp1 = new Products("sp1.jpg", "PANASONIC", "Nhật Bản", "Treo tường", "1.0 HP", "1 năm", "9,750,000 đ");
    var sp2 = new Products("sp2.jpg", "PANASONIC", "Thái Lan", "Treo tường", "2.5 HP", "5 năm", "7,350,000 đ");
    var sp3 = new Products("sp3.jpg", "PANASONIC", "Hàn Quốc", "Treo tường", "2.0 HP", "1 năm", "10,950,000 đ");
    var sp4 = new Products("sp4.jpg", "TOSHIBA", "Hàn Quốc", "Treo tường", "1.0 HP", "1 năm", "9,600,000 đ");
    var sp5 = new Products("sp5.jpg", "TOSHIBA", "Nhật Bản", "Treo tường", "1.5 HP", "2 năm", "13,000,000 đ");
    var sp6 = new Products("sp6.jpg", "TOSHIBA", "Trung Quốc", "Treo tường", "1.5 HP", "1 năm", "11,700,000 đ");
    var sp7 = new Products("sp7.jpg", "SAMSUNG", "Nhật Bản", "Treo tường", "2.0 HP", "3 năm", "11,600,000 đ ");
    var sp8 = new Products("sp8.jpg", "SAMSUNG", "Trung Quốc", "Treo tường", "1.5 HP", "1 năm", "10,200,000 đ ");
    var sp9 = new Products("sp9.jpg", "SAMSUNG", "Hàn Quốc", "Treo tường", "1.0 HP", "2 năm", "9,000,000 đ ");

    compare.push(sp1);
    compare.push(sp2);
    compare.push(sp3);
    compare.push(sp4);
    compare.push(sp5);
    compare.push(sp6);
    compare.push(sp7);
    compare.push(sp8);
    compare.push(sp9);
});

function changeProducts1() {
    var x = document.getElementById("products-1");
    var i = x.selectedIndex;
    var contentSp = document.getElementById("contentSp1");
    $(contentSp).empty();
    for(var j = 0 ; j<compare.length ; j++){
        if(i == j + 3){
            contentSp.innerHTML += '<div class="anhsp">'
                + '<img src="images/sosanh/' + compare[j].Anh +'"/>'
                + '</div>'
                + '<div>'
                + '<div class="box">' + compare[j].NhanHieu + '</div>'
                + '<div class="box">' + compare[j].XuatSu + '</div>'
                + '<div class="box">' + compare[j].ChungLoai + '</div>'
                + '<div class="box">' + compare[j].CongSuat + '</div>'
                + '<div class="box">' + compare[j].BaoHanh + '</div>'
                + '<div class="box">' + compare[j].Gia + '</div>'
                + '</div>'
            ;
        }
    }
}

function changeProducts2() {
    var y = document.getElementById("products-2");
    var z = y.selectedIndex;
    var contentSp = document.getElementById("contentSp2");
    $(contentSp).empty();
    for(var j = 0 ; j<compare.length ; j++){
        if(z == j + 3){
            contentSp.innerHTML += '<div class="anhsp">'
                + '<img src="images/sosanh/' + compare[j].Anh +'"/>'
                + '</div>'
                + '<div>'
                + '<div class="box">' + compare[j].NhanHieu + '</div>'
                + '<div class="box">' + compare[j].XuatSu + '</div>'
                + '<div class="box">' + compare[j].ChungLoai + '</div>'
                + '<div class="box">' + compare[j].CongSuat + '</div>'
                + '<div class="box">' + compare[j].BaoHanh + '</div>'
                + '<div class="box">' + compare[j].Gia + '</div>'
                + '</div>'
            ;
        }
    }
}