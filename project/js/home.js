/**
 * Created by iL22 on 11/13/2018.
 */


//  Load Products
var saleProductsHome = [];
var sellerProductsHome = [];
function ProductsHome(classBrand,image,brand,name,li1,li2,li3,modal,dataName,dataPrice) {
    this.ClassBrand = classBrand;
    this.Image = image;
    this.Brand = brand;
    this.Name = name;
    this.Li1 = li1;
    this.Li2 = li2;
    this.Li3 = li3;
    this.Modal = modal;
    this.DataName = dataName;
    this.DataPrice = dataPrice;
}
var p1 = new ProductsHome("panasonic","pana_2_new.jpg","PANASONIC","Máy lạnh treo tường Panasonic CS-U9TKH-8 Gas R32 inverter model 2017","Làm Lạnh Nhanh","Làm Lạnh Dễ Chịu","Đường Nét Mảnh Mai & Bóng Bẩy","#myModal_6","CS-U9TKH-8","10200000");
var p2 = new ProductsHome("toshiba","lg_3_sale.jpg","TOSHIBA","Máy lạnh âm trần ATNQ36GNLE6 4hp 36000btu","Lan tỏa khắp mọi nơi","Dàn tản nhiệt được tối đa hóa chống ăn mòn","Lắp đặt nhanh chóng dễ dàng","#myModal_13","ATNQ36GNLE6","30400000");
var p3 = new ProductsHome("panasonic","pana_3.1_best.jpg","PANASONIC","Máy lạnh treo tường Panasonic CS-PU9TKH-8 Gas R32 model 2017","Tiết kiệm điện bằng cách thay đổi tốc độ quay của máy nén.","Kiểm soát nhiệt độ chính xác","Công nghệ Nanoe-G giúp mang lại không khí trong lạnh","#myModal_8","CS-PU9TKH-8","9000000");
var p4 = new ProductsHome("panasonic","pana_2_new.jpg","PANASONIC","Máy lạnh treo tường Panasonic CS-U9TKH-8 Gas R32 inverter model 2017","Làm Lạnh Nhanh","Làm Lạnh Dễ Chịu","Đường Nét Mảnh Mai & Bóng Bẩy","#myModal_6","CS-U9TKH-8","10200000");
var p5 = new ProductsHome("toshiba","lg_3_sale.jpg","TOSHIBA","Máy lạnh âm trần ATNQ36GNLE6 4hp 36000btu","Lan tỏa khắp mọi nơi","Dàn tản nhiệt được tối đa hóa chống ăn mòn","Lắp đặt nhanh chóng dễ dàng","#myModal_13","ATNQ36GNLE6","30400000");
var p6 = new ProductsHome("panasonic","pana_3.1_best.jpg","PANASONIC","Máy lạnh treo tường Panasonic CS-PU9TKH-8 Gas R32 model 2017","Tiết kiệm điện bằng cách thay đổi tốc độ quay của máy nén.","Kiểm soát nhiệt độ chính xác","Công nghệ Nanoe-G giúp mang lại không khí trong lạnh","#myModal_8","CS-PU9TKH-8","9000000");

var n1 = new ProductsHome("samsung","daikin_3.1_best.jpg","SAMSUNG","Máy lạnh Samsung treo tường FTKC35TVMV Inverter Gas R32","Mắt thần thông minh","Điều khiển thông minh","Cánh tản nhiệt dàn nóng được xử lý chống ăn mòn","#myModal_7","FTKC35TVMV","11700000");
var n2 = new ProductsHome("toshiba","lg_5_best.jpg","TOSHIBA","Máy lạnh điều hòa treo tường TOSHIBA V13APD 1.5hp inverter","Máy nén Smart Inverter","Tấm vi lọc bảo vệ đa năng 3M","Tiết Kiệm Điện Năng","#myModal_9","V13APD","9100000");
var n3 = new ProductsHome("toshiba","lg_6_best.jpg","TOSHIBA","Máy lạnh điều hòa treo tường V10APF 1hp inverter dualcool","Cánh quạt xiên độc quyền và công nghệ động cơ BLDC tiên tiến","Chế độ vận hành khi ngủ","Gió thổi xa 9M","#myModal_10","V10APF","11800000");
var n4 = new ProductsHome("samsung","daikin_3.1_best.jpg","SAMSUNG","Máy lạnh Samsung treo tường FTKC35TVMV Inverter Gas R32","Mắt thần thông minh","Điều khiển thông minh","Cánh tản nhiệt dàn nóng được xử lý chống ăn mòn","#myModal_7","FTKC35TVMV","11700000");
var n5 = new ProductsHome("toshiba","lg_5_best.jpg","TOSHIBA","Máy lạnh điều hòa treo tường TOSHIBA V13APD 1.5hp inverter","Máy nén Smart Inverter","Tấm vi lọc bảo vệ đa năng 3M","Tiết Kiệm Điện Năng","#myModal_9","V13APD","9100000");
var n6 = new ProductsHome("toshiba","lg_6_best.jpg","TOSHIBA","Máy lạnh điều hòa treo tường V10APF 1hp inverter dualcool","Cánh quạt xiên độc quyền và công nghệ động cơ BLDC tiên tiến","Chế độ vận hành khi ngủ","Gió thổi xa 9M","#myModal_10","V10APF","11800000");

saleProductsHome.push(p1);
saleProductsHome.push(p2);
saleProductsHome.push(p3);
saleProductsHome.push(p4);
saleProductsHome.push(p5);
saleProductsHome.push(p6);

sellerProductsHome.push(n1);
sellerProductsHome.push(n2);
sellerProductsHome.push(n3);
sellerProductsHome.push(n4);
sellerProductsHome.push(n5);
sellerProductsHome.push(n6);
localStorage.SaleProductsHome = JSON.stringify(saleProductsHome);
localStorage.SellerProductsHome = JSON.stringify(sellerProductsHome);

window.onload = function () {
    saleProductsHome = JSON.parse(localStorage.SaleProductsHome);
    sellerProductsHome = JSON.parse(localStorage.SellerProductsHome);
    var contentSale = document.getElementById("products-sale");
    var contentSeller = document.getElementById("products-seller");
    var test = document.getElementById("test");

    /*for ( var i in saleProductsHome){
        contentSale.innerHTML += '<div class="owl-content">'
            + '<div class="thumbnail nhap ' + saleProductsHome[i].ClassBrand + '">'
            + '<div class="hang">'
            + '<img src="images/sanpham/' + saleProductsHome[i].Image + '" class="img-responsive"/>'
            + '<div>'+ saleProductsHome[i].Brand  + '</div>'
            + '</div>'
            + '<div class="new-img-2"><img src="images/sanpham/sale2.png" height="90" width="90"/></div>'
            + '<div class="content-pr">'
            + '<h1 class="text-center  " style="font-size: large">' + saleProductsHome[i].Name + '</h1>'
            + '<hr width="80%" align="center" style="margin-left: 10%"/>'
            + '<ul class="text-left">'
            + '<li>' + saleProductsHome[i].Li1 + '</li>'
            + '<li>' + saleProductsHome[i].Li2 + '</li>'
            + '<li>' + saleProductsHome[i].Li3 + '</li>'
            + '</ul>'
            + '</div>'
            + '<button type="button" class="btn buttonClose " data-toggle="modal" data-target="'+ saleProductsHome[i].Modal + '"> XemThêm </button>'
            + '<button class="btn buttonMuaHang addToCart" data-name="' + saleProductsHome[i].DataName + '" data-price="' + saleProductsHome[i].DataPrice + '">Mua Hàng</button><br/>'
            + '<div class="diadiem">'
            + '<a href="#" style="text-decoration: none"><span class="glyphicon glyphicon-star" aria-hidden="true"></span>Mua ở đâu</a>'
            + '</div>'
            + '</div>'
        ;
    }*/


    /*for ( var j in sellerProductsHome){
        contentSeller.innerHTML +=
        '<div class="owl-content">'
            +'<div class="thumbnail nhap panasonic">'
                +'<div class="hang ">'
                    +'<img src="images/sanpham/pana_3.1_best.jpg" class="img-responsive"/>'
                    +'<div>PANASONIC</div>'
                +'</div>'
                +'<div class="new-img-2"><img src="images/sanpham/sale2.png" height="90" width="90"/></div>'
                    +'<div class="content-pr">'
                    +'<h1 class="text-center  " style="font-size: large">Máy lạnh treo tường Panasonic CS-PU9TKH-8 Gas R32 model 2017</h1>'
                    +'<hr width="80%" align="center" style="margin-left: 10%"/>'
                    +'<ul class="text-left">'
                    +'<li> Tiết kiệm điện bằng cách thay đổi tốc độ quay của máy nén.</li>'
                    +'<li>Kiểm soát nhiệt độ chính xác</li>'
                    +'<li>Công nghệ Nanoe-G giúp mang lại không khí trong lạnh</li>'
                    +'</ul>'
                +'</div>'
                +'<button type="button" class="btn buttonClose " data-toggle="modal" data-target="#myModal_8">'
                     + 'Xem Thêm'
                +'</button>'
                +'<button class="btn buttonMuaHang  addToCart " data-name="CS-PU9TKH-8" data-price="9000000">'
            + 'Mua Hàng'
            +'</button>'
                +'<br/>'
                +'<div class="diadiem">'
                   +' <a href="#" style="text-decoration: none"><span class="glyphicon glyphicon-star" aria-hidden="true"></span>Mua ở đâu</a>'
                +'</div>'
            +'</div>'
        +'</div>'
        ;
    }*/
};/*end load products*/
$(document).ready(function (){
    /*danh muc*/
    $("#s1").click(function () {
        $("ul#s1 li").toggle(500);
    });
    $("#s2").click(function () {
        $("ul#s2 li").toggle(500);
    });
    $("#s3").click(function () {
        $("ul#s3 li").toggle(500);
    });
    $("#danhmuc").click(function () {
        $("#s1").toggle(500);
        $("#s2").toggle(500);
        $("#s3").toggle(500);
        $("#s4").toggle(500);
    });/* end danh muc*/



    /*Tab*/
    $(".tabs-menu a").click(function(event) {
        event.preventDefault();
        $(this).parent().addClass("current");
        $(this).parent().siblings().removeClass("current");
        var tab = $(this).attr("href");
        $(".tab-content").not(tab).css("display", "none");
        $(tab).fadeIn();
    });/*end Tab*/

    /* Owl carousel */
    $(".owl-carousel").owlCarousel({
        slideSpeed : 500,
        rewindSpeed : 500,
        mouseDrag : true,
        stopOnHover : true
    });
    /* Own navigation */
    $(".owl-nav-prev").click(function(){
        $(this).parent().next(".owl-carousel").trigger('owl.prev');
    });
    $(".owl-nav-next").click(function(){
        $(this).parent().next(".owl-carousel").trigger('owl.next');
    });
});


